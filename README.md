# Node support  - Pulumi Cluster

## Getting started

Nomadic Labs provides a free to access cluster of nodes on mainnet. This tutorial is inspired by [Oxhead Alpha's medium article](https://medium.com/the-aleph/deploy-scalable-tezos-nodes-in-the-cloud-bbe4f4f4ddcc)

## Clone the repository and then access it:

```
cd node-support-pulumi-cluster

```

## Requirements 

- [AWS account and CLI tool](https://aws.amazon.com/cli/?nc1=h_ls)
- (optional)[Pulumi account and CLI tool](https://www.pulumi.com/docs/get-started/install/)
 (It is possible to use [alternative backend to store the pulumi states, for example on AWS S3](https://www.pulumi.com/docs/intro/concepts/state/))
- [Nodejs](https://nodejs.org/en/download/package-manager/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- (optional)[k9s](https://github.com/derailed/k9s) (as a local CLI dashboard)
- (optional)[Lens](https://k8slens.dev/) (as a local dashboard alternative to k9s)

## Choose your backend


This will choose pulumi as default backend
```
pulumi login
```

For using AWS S3 as backend, make sure you have the proper rights
```
pulumi login 's3://<URL of your bucket>'
```
For example, if your bucket is called `nomadic-pulumi-states` with a subfolder `pulumi-node-cluster` in `us-east-2` region:
```
pulumi login 's3://nomadic-pulumi-states/pulumi-node-cluster?region=us-east-2'
```

## Choose a snapshot file (available [here](https://xtz-shots.io/mainnet/)) and a network (in our case mainnet)

The following values must be updated to switch to the right network (in the file `values.yml`):

```
rolling_snapshot_url: https://mainnet-v15.xtz-shots.io/mainnet-3201758.rolling
```

```
node_config_network:
  chain_name: mainnet

images:
  octez: tezos/tezos:v15-release  
```

## Launch the cluster

Before launching the cluster, give [pulumi's path](https://www.pulumi.com/docs/get-started/install/#installing-pulumi) to your current shell: 

 `export PATH="$PATH:~/.pulumi/bin"`

Then execute (you'll probably be asked to choose a region to deploy, use: `pulumi config set region`):

 `pulumi up`
 
## Use a specific DNS as an endpoint (on AWS)

To route your DNS,  access the [Route53](https://aws.amazon.com/route53/) configuration page, browse you load balancer url and link it to your DNS. 


## Lens configuration

We have chosen Lens as a monitoring tool. 

Create the kubeconfig file to provide to Lens:
     
`pulumi stack output kubeconfig --show-secrets --json > kubeconfig.json`
 
Then, use this file in Lens.

## Test the cluster

Our domain name is `mainnet.nomadic-labs.com`

We can perform any type of requests on our cluster. For example, let's ask for the chain_id:
 
`curl http://mainnet.nomadic-labs.com/chains/main/chain_id`

## Upgrades 

To upgrade the cluster (on mainnet), you need to modify former `octez: tezos/tezos:v15-release` and replace with the latest octez image version.
 
## Important

This cluster needs specific rights to grant access. See the screen shots is the **config_pic** repository.
  

